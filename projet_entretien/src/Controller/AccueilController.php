<?php
// src/Controller/AccueilController.php

namespace App\Controller;

use App\Repository\ActualiteRepository;
use App\Form\ActualiteType;
use App\Entity\Actualite;
use Twig\Environment;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class AccueilController  extends AbstractController{

  /**
  * @var ActualiteRepository
  */
  private $repository;

  /**
  * @var ObjectManager
  */
  private $em;

  public function __construct(ActualiteRepository $repository, ObjectManager $em){
    $this->repository = $repository;
    $this->em = $em;
  }


  public function index(Environment $twig){

    $actu = $this->repository->findOneBy(array('id' => '1'));

    return $this->render('Accueil/index.html.twig',array(
      'actualite' => $actu
    ));
  }




}
