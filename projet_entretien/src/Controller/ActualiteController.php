<?php

namespace App\Controller;

use App\Entity\Actualite;
use App\Repository\ActualiteRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\Common\Persistence\ObjectManager;
use Twig\Environment;



class ActualiteController extends AbstractController{

  /**
  * @var ActualiteRepository
  */
  private $repository;

  /**
  * @var ObjectManager
  */
  private $em;

  public function __construct(ActualiteRepository $repository, ObjectManager $em){
    $this->repository = $repository;
    $this->em = $em;
  }


  public function index(Environment $twig){

    $actus = $this->repository->findAll();

    return $this->render('Actualites/actualites.html.twig',array(
      'actualites' => $actus
    ));

  }




}
