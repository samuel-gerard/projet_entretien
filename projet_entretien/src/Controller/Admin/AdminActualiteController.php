<?php

namespace App\Controller\Admin;

use App\Repository\ActualiteRepository;
use App\Form\ActualiteType;
use App\Entity\Actualite;
use Twig\Environment;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;



class AdminActualiteController extends AbstractController{

  /**
  * @var ActualiteRepository
  */
  private $repository;

  /**
  * @var ObjectManager
  */
  private $em;

  public function __construct(ActualiteRepository $repository, ObjectManager $em){
    $this->repository = $repository;
    $this->em = $em;
  }


  public function index(Environment $twig){
    $actus = $this->repository->findAll();
    $this->em->flush();
    return $this->render('Actualites/actualite-edit.html.twig',array(
      'actualites' => $actus
    ));
  }


  /**
  * @return \Symfony\Component\HttpFoundation\Response
  */
  public function edit($id, Request $request){
    $actu = $this->repository->findOneBy(['id' => $id]);
    $form = $this->createForm(ActualiteType::class, $actu);
    $form->handleRequest($request);
    if($form->isSubmitted() && $form->isValid()){
      //$this->em->persist($actu);
      $this->em->flush();
      $this->addFlash('success', 'L\'actualité  a été modifiée avec succès.');
      return $this->redirectToRoute('admin.actualite.index');
    }
    return $this->render('Actualites/edit.html.twig', array(
      'form' => $form->createView()
    ));
  }

  public function create(Request $request){
    $actu = new Actualite();
    $form = $this->createForm(ActualiteType::class, $actu);
    $form->handleRequest($request);
    if($form->isSubmitted() && $form->isValid()){
      $this->em->persist($actu);
      $this->em->flush();
      $this->addFlash('success', 'L\'actualité  a été publiée avec succès.');
      return $this->redirectToRoute('admin.actualite.index');
    }
    return $this->render('Actualites/create.html.twig', array(
      'actualite' => $actu,
      'form' => $form->createView()
    ));
  }


  public function delete($id, Request $request){
    $actu = $this->repository->findOneBy(['id' => $id]);
    $this->em->remove($actu);
    $this->em->flush();
    $this->addFlash('success', 'L\'actualité  a été supprimée avec succès.');
    return $this->redirectToRoute('admin.actualite.index');
  }

}


 ?>
