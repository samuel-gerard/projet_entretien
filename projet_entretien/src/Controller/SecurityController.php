<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Form\RegistrationType;
use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;


class SecurityController extends AbstractController {

  private $encoder;

  public function login(AuthenticationUtils $authenticationUtils){
    $error = $authenticationUtils->getLastAuthenticationError();
    $lastUsername = $authenticationUtils->getLastUsername();
    return $this->render('Security/login.html.twig', [
      'last_username' => $lastUsername,
      'error' => $error
    ]);
  }

  public function inscription(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $passwordEncoder){
    $user = new User();
    $form = $this->createForm(RegistrationType::class, $user);
    $form->handleRequest($request);

    if($form->isSubmitted() && $form->isValid()){
      $password = $passwordEncoder->encodePassword($user, $user->getPassword());
      $user->setPassword($password);
      $manager->persist($user);
      $manager->flush();
      $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
      $this->container->get('security.token_storage')->setToken($token);
      $this->container->get('session')->set('_security_main', serialize($token));
      $this->addFlash('success', 'Vous êtes maintenant inscrit !');
      return $this->redirectToRoute('admin.actualite.index');
    }
    return $this->render('Security/inscription.html.twig', array(
      'form' => $form->createView()
    ));
  }
















}
